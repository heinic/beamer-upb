
all: main.pdf doc
.PHONY: all

main.pdf: main.tex
	make -C template

#	pdflatex main.tex
#	pdflatex main.tex
	xelatex main.tex
	xelatex main.tex

doc:
	make -C doc/
.PHONY: doc
