%% Copyright (C) 2018   Nico Heitmann
%% This file may be distributed and/or modified under the terms of the
%% GNU General Public License.
%%
%% See the file LICENSE for more details.


\documentclass[a4paper, 12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[sfdefault]{FiraSans}

\usepackage[margin=2cm]{geometry}

\usepackage{metalogo}
\usepackage{hyperref}

\usepackage{tikz}

\usepackage{xcolor}
\input{../template/colors.tex}

\begin{document}

\thispagestyle{empty}

\null\vfill
\begin{center}
  {\Huge\bfseries\scshape User Guide}
  \vskip2em
  {\Huge Presentation Template}\\
  \textit{for the University of Paderborn}
  \vskip3em
  {\scshape Nico Heitmann}\\
  \textit{nico.heitmann01@gmx.de}
\end{center}
\vfill

\newpage

\tableofcontents

\section*{Preface}
This template for beamer slides was created for a contest at the
university of Paderborn. It is mostly structured like a regular beamer
theme, but also contains additional graphics as part of the theme.  It
has been tested on a Linux, with \TeX\ Live 2017 and \TeX\ Live 2019
(dev), and should work on all unix-like operating systems.

This user guide is not an exhaustive manual, it focuses on the
preparation and the practical aspects using this template.

For information on beamer in general, you can take a look at the
beamer user guide.


\section{Preparation}
Due to license restrictions, the university's fonts cannot be bundled
with this template. Because of this, you may to download these before
using this template.

\subsection{Fonts}
The official fonts of the university of Paderborn are \verb+FF Meta+,
\verb+Arial Narrow+ and \verb+Arial+. None of these fonts are standard
packages in \TeX\ Live.

If you have these fonts installed, you can use \XeLaTeX\ or
\LuaLaTeX\ to compile, as these engines can use OpenType and TrueType
fonts.

\verb+Arial+ and \verb+Arial Narrow+ may be preinstalled on your
system. If you are employed by the university, you can also get a copy
of \verb+FF Meta+ from the
university\footnote{\url{https://www.uni-paderborn.de/en/university/marketing/design-vorgaben-templates/fonts/}}.

Because these fonts cannot be bundled, the template uses
\verb+FiraSans+ by default. This is a font derived from \verb+FF Meta+, but
is freely avaliable as a package for \TeX.

\subsection{Background images}
The background images are generated from a \LaTeX\ source file. To
compile the background images, you should run \verb+make+ in the
\verb+template/+ directory at least once. The Makefile in the main
directory also ensures that the background is up to date.

The background images are compatible with 4:3 and 16:9 aspect ratios,
making your slides 4:3 (the default) is recommended.

\subsection{\LaTeX\ packages}
This template relies on several \LaTeX\ packages, but aside from the
font \verb+FiraSans+, these should be default packages in your
\TeX\ distribution. If you see errors about missing \verb+.cls+ or
\verb+.sty+ files, you can install the respective packages
individually. Alternatively, you may want to install larger sets of
packages beforehand to have them preinstalled in the future.

All of the packages used are part of \TeX\ Live.

\section{Usage}
This template behaves mostly like a regular beamer style. Because of
this, migrating existing presentations should require little change to
use this theme.

\subsection{Including the template}
The template exspects to be placed in a directory called
\verb+template/+ relative to the working directory of the
\LaTeX\ engine. You can copy the template directory or symlink to it
from your presentation.

The template can be included with the \verb+\input+-command. For the
default location this would be \verb+\input{template/template.tex}+

If you would like to input the template from a different folder, you
need to define an additional macro before including the template, so
that the \TeX-files and graphics can be included:
\\ \verb+\newcommand\upbTemplatePath{+$\left<template\ folder\right>$\verb+}+
\\ where the $\left<template\ folder\right>$ is \verb+template/+ by
default. The $\left<template\ folder\right>$ has to end with a forward
slash.

\subsection{University logo}
The logo is avaliable in both german and english. Both versions are
included as pdf files in this template and can be chosen using
\verb+\logovariant{+$\left<en/de\right>$\verb+}+

For use in a presentation, you may need to get permission to use the
logo of the university.

The logo can also be set using beamer's \verb+\logo+ macro and should
have a height of \verb+0.65cm+. By default, the logo will be scaled up
on the title page. The logo inserted on the title page can be changed
individually by overwriting the \verb+\titlelogo+ macro.

\subsection{Title page}
The title page uses the variables defined in the preamble of the
document.

The frame of the title page has to be preceeded by
\verb+\titlepagetrue+ and before the next frame, there needs to be a
\verb+\titlepagefalse+. These commands are used to adjust the
background and outer area of that slide.

Each line of the title is set in a colored box. You can specify the
background with
\verb+\setbeamercolor{title background}{bg=+$\left<color\right>$\verb+}+. By
default, the title is spilt into lines on each occurance of
\verb+\\+. Do not place a newline at the end of the title. If you wish
to change this behaviour (e.g. to omit the newlines in the title), you
can replace the beamer template \verb+title page title+. The colored
lines of the title can be inserted using
\verb+\titleline{+$\left<line\right>$\verb+}+

The contents of the template \verb+title page picture+ are inserted in
the upper right corner of the title page. You can use the image of the
official PowerPoint template, however this image is not licensed
freely and thus cannot be included in this template.

If a subtitle is specified, it will be placed below the main title. If
there is no subtitle, the author will be moved up from the footline.

\section{Colors}
This template defines names for the official
colors\footnote{\url{https://www.uni-paderborn.de/en/university/marketing/design-vorgaben-templates/colors/}\\More
  info at:
  \url{https://www.uni-paderborn.de/fileadmin/marketing/corporate-design/Farben/UPB_Farbdefinition_2018_01.pdf}
  (german)} of the university. All of these additional colors have a
name starting with \verb+upb+.

There are one primary color and three secondary colors. There are also
five additional colors. The additional colors can be used as accent
colors. The background of the title can be specified by changing the
background of the beamer color named \verb+title line background+

You may want to change the accent color for individual slides or
sections of the presentation. To accomplish this, set the beamer color
named \verb+accent+ to the appropriate color in between two frames
using \verb+\setbeamercolor{accent}{fg=+$\left<color\right>$\verb+}+. The
following frames will have the new accent color.

The colors \verb+alerted text+ and \verb+structure+ inherit from
\verb+accent+ by default, but they can also be overridden
individually.

\def\colorpreview#1{\tikz \draw (0, 0) node[anchor=south, fill=#1, inner sep=1em, minimum width=8em, minimum height=5em] {} ++(0, 0.5em) node[anchor=south, fill=white, minimum width=7em]{\strut #1};}

\begin{center}%
  \noindent%
  \colorpreview{\upbColorPrimary}%
  \hskip 8em%
  \foreach \color in \upbColorsSecondary {\colorpreview{\color}}
  \medskip
  \noindent%
  \foreach \color in \upbColorsAdditional {\colorpreview{\color}}%
\end{center}

\end{document}
